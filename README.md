# Ansible role to manage SASL file databases

## Introduction

This role can create and delete users in a Cyrus SASL file database.

This role does not install or care about the saslauthd daemon. You do
not need it to access a 'sasldb' file using the 'auxprop' method for
example. This need is left to another role.

## Variables

- **db_file**: database file to edit (defaults to '/etc/sasldb2')
- **db_group**: group of the database file (defaults to 'root')
- **user_delete**: list of user names to delete 
- **user_create**: list of users to create, each being a small structure:
    + user: user name
    + pw: user password
    + domain: account domain (defaults to the host's domain name)
    + application: application name (*MUST* match the name used by the program accessing this database)

